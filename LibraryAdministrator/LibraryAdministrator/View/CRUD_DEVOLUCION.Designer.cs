﻿
namespace LibraryAdministrator.View
{
    partial class CRUD_DEVOLUCION
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.DGVDevolucion = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.btnDevolver = new System.Windows.Forms.Button();
            this.DGVPrestamos = new System.Windows.Forms.DataGridView();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.DGVDevolucion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DGVPrestamos)).BeginInit();
            this.SuspendLayout();
            // 
            // DGVDevolucion
            // 
            this.DGVDevolucion.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGVDevolucion.Location = new System.Drawing.Point(454, 103);
            this.DGVDevolucion.Name = "DGVDevolucion";
            this.DGVDevolucion.Size = new System.Drawing.Size(334, 235);
            this.DGVDevolucion.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(284, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(225, 24);
            this.label1.TabIndex = 1;
            this.label1.Text = "Tabla de Devoluciones";
            // 
            // btnDevolver
            // 
            this.btnDevolver.BackgroundImage = global::LibraryAdministrator.Properties.Resources.music_placeholder_album1;
            this.btnDevolver.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDevolver.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDevolver.ForeColor = System.Drawing.SystemColors.Control;
            this.btnDevolver.Location = new System.Drawing.Point(358, 185);
            this.btnDevolver.Name = "btnDevolver";
            this.btnDevolver.Size = new System.Drawing.Size(90, 38);
            this.btnDevolver.TabIndex = 2;
            this.btnDevolver.Text = "Devolver";
            this.btnDevolver.UseVisualStyleBackColor = true;
            this.btnDevolver.Click += new System.EventHandler(this.btnDevolver_Click);
            // 
            // DGVPrestamos
            // 
            this.DGVPrestamos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGVPrestamos.Location = new System.Drawing.Point(12, 103);
            this.DGVPrestamos.Name = "DGVPrestamos";
            this.DGVPrestamos.Size = new System.Drawing.Size(340, 235);
            this.DGVPrestamos.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(134, 392);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(567, 18);
            this.label2.TabIndex = 4;
            this.label2.Text = "Seleccione un campo de esta tabla (Izquierda) y presione el boto Devolver";
            // 
            // CRUD_DEVOLUCION
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::LibraryAdministrator.Properties.Resources.music_placeholder_album1;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.DGVPrestamos);
            this.Controls.Add(this.btnDevolver);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.DGVDevolucion);
            this.Name = "CRUD_DEVOLUCION";
            this.Text = "CRUD_DEVOLUCION";
            this.Load += new System.EventHandler(this.CRUD_DEVOLUCION_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DGVDevolucion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DGVPrestamos)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView DGVDevolucion;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnDevolver;
        private System.Windows.Forms.DataGridView DGVPrestamos;
        private System.Windows.Forms.Label label2;
    }
}