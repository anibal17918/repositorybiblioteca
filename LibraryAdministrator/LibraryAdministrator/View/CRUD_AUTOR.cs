﻿using LibraryAdministrator.Core;
using LibraryAdministrator.DBModel.entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LibraryAdministrator.Interfaces;
using LibraryAdministrator.Repository;

namespace LibraryAdministrator.View
{
    public partial class CRUD_AUTOR : Form
    {
        IAutorRepository _autorRepository = new AutorRepository();
        
        public CRUD_AUTOR()
        {
            InitializeComponent();
        }

        private void CRUD_AUTOR_Load(object sender, EventArgs e)
        {
            dataGridAutor.DataSource = _autorRepository.GetAll().Select(p => new {p.id, p.Nombre, p.Apellido, p.FechaRegistro, p.FechaModificacion}).ToList();
            
        }
        //Boton para crear Autor
        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                _autorRepository.Create(new Autor()
                {
                    Nombre = txt_Nombre.Text,
                    Apellido = txt_Apellido.Text,
                    Nacionalidad = txt_Nacionalidad.Text,
                    borrado = false,
                    Estatus = txt_Estatus.Text,
                    FechaRegistro = DateTime.Today,
                    FechaModificacion = DateTime.Today

                });
                MessageBox.Show("Fue Creado Exitosamente");
                Cargar();
            }
            catch (Exception)
            {
                MessageBox.Show("Algo salio mal");

                throw;
            }
        }

        private void Cargar() { dataGridAutor.DataSource = _autorRepository.GetAll().Select(p => new { p.id, p.Nombre, p.Apellido, p.FechaRegistro, p.FechaModificacion }).ToList(); }
        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }

        private void btn_Borrar_Click(object sender, EventArgs e)
        {
            try
            {
                _autorRepository.Delete(_autorRepository.FindbyID(int.Parse(txt_ID.Text)));
                MessageBox.Show("Fue Borrado Exitosamente");
                Cargar();
            }
            catch (Exception)
            {
                MessageBox.Show("Inserte el id, solo incluye números / El id no fue encontrado");
            }
        }

        private void txt_ID_TextChanged(object sender, EventArgs e)
        {

        }

        private void btn_Actualizar_Click(object sender, EventArgs e)
        {
            try
            {
                Autor autor = _autorRepository.FindbyID(int.Parse(txt_ID.Text));

                autor.Nombre = txt_Nombre.Text;
                autor.Apellido = txt_Apellido.Text;
                autor.Nacionalidad = txt_Nacionalidad.Text;
                autor.Estatus = txt_Estatus.Text;
                autor.FechaModificacion = DateTime.Today;

                if (MessageBox.Show("Desea actualizar este Autor", "Actualizar", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    _autorRepository.Update(autor);
                    MessageBox.Show("fue actualizado exitosamente");
                    Cargar();

                }
                else
                {
                    MessageBox.Show("No se realizo ninguna accion");

                }
                
            }
            catch (Exception)
            {
                MessageBox.Show("inserte el id solo incluye números / el id no fue encontrado");
            }
        }

        private void btn_Refrescar_Click(object sender, EventArgs e)
        {
            try
            {
                int a;
                bool isNumeric = int.TryParse(txt_ID.Text, out a);
                if (isNumeric)
                {
                    //para saber si es un numero (int)
                }
                else
                {
                    MessageBox.Show("Inserte un ID valido");
                }

                try
                {
                    Autor autor = _autorRepository.FindbyID(int.Parse(txt_ID.Text));

                    foreach (DataGridViewRow row in dataGridAutor.Rows)
                    {
                        if (row.Cells[0].Value.ToString().Contains(txt_ID.Text))
                        {
                            dataGridAutor.DataSource = _autorRepository.GetAll().Select(p => new { p.id, p.Nombre, p.Apellido, p.FechaRegistro, p.FechaModificacion })
                                .Where(p => p.id == autor.id).ToList();
                        }
                        else
                        {
                            Cargar();
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("ID no encontrado" + ex.ToString());

                }
            }
            catch (FormatException ex)
            {
                MessageBox.Show("Something went wrong " + ex.ToString());
            }
        }

        private void dataGridAutor_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            string nombre = txt_Nombre.Text;
            dataGridAutor.DataSource = _autorRepository.GetAll().Where(p => p.Nombre.Contains(nombre)).
                Select(p => new { p.id, p.Nombre, p.Apellido, p.FechaRegistro, p.FechaModificacion }).ToList();
        }
    }
}
