﻿namespace LibraryAdministrator.View
{
    partial class CRUD_Libro
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_Nombre = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cmb_Autor = new System.Windows.Forms.ComboBox();
            this.cmb_Editorial = new System.Windows.Forms.ComboBox();
            this.DGV_Libro = new System.Windows.Forms.DataGridView();
            this.txt_Nombre = new System.Windows.Forms.TextBox();
            this.btn_Crear = new System.Windows.Forms.Button();
            this.btn_Actualizar = new System.Windows.Forms.Button();
            this.btn_Borrar = new System.Windows.Forms.Button();
            this.btnBuscar = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.DGV_Libro)).BeginInit();
            this.SuspendLayout();
            // 
            // lbl_Nombre
            // 
            this.lbl_Nombre.AutoSize = true;
            this.lbl_Nombre.BackColor = System.Drawing.Color.Transparent;
            this.lbl_Nombre.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Nombre.ForeColor = System.Drawing.SystemColors.Control;
            this.lbl_Nombre.Location = new System.Drawing.Point(12, 40);
            this.lbl_Nombre.Name = "lbl_Nombre";
            this.lbl_Nombre.Size = new System.Drawing.Size(85, 24);
            this.lbl_Nombre.TabIndex = 0;
            this.lbl_Nombre.Text = "Nombre";
            this.lbl_Nombre.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.Control;
            this.label2.Location = new System.Drawing.Point(12, 83);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 24);
            this.label2.TabIndex = 1;
            this.label2.Text = "Autor";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.Control;
            this.label1.Location = new System.Drawing.Point(12, 126);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(86, 24);
            this.label1.TabIndex = 2;
            this.label1.Text = "Editorial";
            // 
            // cmb_Autor
            // 
            this.cmb_Autor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_Autor.FormattingEnabled = true;
            this.cmb_Autor.Location = new System.Drawing.Point(103, 86);
            this.cmb_Autor.Name = "cmb_Autor";
            this.cmb_Autor.Size = new System.Drawing.Size(329, 21);
            this.cmb_Autor.TabIndex = 3;
            this.cmb_Autor.SelectedIndexChanged += new System.EventHandler(this.cmb_Autor_SelectedIndexChanged);
            // 
            // cmb_Editorial
            // 
            this.cmb_Editorial.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_Editorial.FormattingEnabled = true;
            this.cmb_Editorial.Location = new System.Drawing.Point(104, 129);
            this.cmb_Editorial.Name = "cmb_Editorial";
            this.cmb_Editorial.Size = new System.Drawing.Size(328, 21);
            this.cmb_Editorial.TabIndex = 4;
            this.cmb_Editorial.SelectedIndexChanged += new System.EventHandler(this.cmb_Editorial_SelectedIndexChanged);
            // 
            // DGV_Libro
            // 
            this.DGV_Libro.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGV_Libro.Location = new System.Drawing.Point(16, 192);
            this.DGV_Libro.Name = "DGV_Libro";
            this.DGV_Libro.Size = new System.Drawing.Size(758, 246);
            this.DGV_Libro.TabIndex = 5;
            // 
            // txt_Nombre
            // 
            this.txt_Nombre.Location = new System.Drawing.Point(103, 44);
            this.txt_Nombre.Name = "txt_Nombre";
            this.txt_Nombre.Size = new System.Drawing.Size(329, 20);
            this.txt_Nombre.TabIndex = 6;
            // 
            // btn_Crear
            // 
            this.btn_Crear.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btn_Crear.BackgroundImage = global::LibraryAdministrator.Properties.Resources.music_background_3;
            this.btn_Crear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Crear.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Crear.ForeColor = System.Drawing.SystemColors.Control;
            this.btn_Crear.Location = new System.Drawing.Point(449, 40);
            this.btn_Crear.Name = "btn_Crear";
            this.btn_Crear.Size = new System.Drawing.Size(146, 51);
            this.btn_Crear.TabIndex = 7;
            this.btn_Crear.Text = "CREAR";
            this.btn_Crear.UseVisualStyleBackColor = false;
            this.btn_Crear.Click += new System.EventHandler(this.btn_Crear_Click);
            // 
            // btn_Actualizar
            // 
            this.btn_Actualizar.BackgroundImage = global::LibraryAdministrator.Properties.Resources.music_placeholder_album1;
            this.btn_Actualizar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Actualizar.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Actualizar.ForeColor = System.Drawing.SystemColors.Control;
            this.btn_Actualizar.Location = new System.Drawing.Point(628, 97);
            this.btn_Actualizar.Name = "btn_Actualizar";
            this.btn_Actualizar.Size = new System.Drawing.Size(146, 51);
            this.btn_Actualizar.TabIndex = 8;
            this.btn_Actualizar.Text = "ACTUALIZAR";
            this.btn_Actualizar.UseVisualStyleBackColor = true;
            this.btn_Actualizar.Click += new System.EventHandler(this.btn_Actualizar_Click);
            // 
            // btn_Borrar
            // 
            this.btn_Borrar.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btn_Borrar.BackgroundImage = global::LibraryAdministrator.Properties.Resources.music_background_8;
            this.btn_Borrar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Borrar.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Borrar.ForeColor = System.Drawing.SystemColors.Control;
            this.btn_Borrar.Location = new System.Drawing.Point(628, 40);
            this.btn_Borrar.Name = "btn_Borrar";
            this.btn_Borrar.Size = new System.Drawing.Size(146, 51);
            this.btn_Borrar.TabIndex = 9;
            this.btn_Borrar.Text = "BORRAR";
            this.btn_Borrar.UseVisualStyleBackColor = false;
            this.btn_Borrar.Click += new System.EventHandler(this.btn_Borrar_Click);
            // 
            // btnBuscar
            // 
            this.btnBuscar.BackgroundImage = global::LibraryAdministrator.Properties.Resources.music_placeholder_album1;
            this.btnBuscar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBuscar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBuscar.ForeColor = System.Drawing.SystemColors.Control;
            this.btnBuscar.Location = new System.Drawing.Point(449, 99);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(146, 51);
            this.btnBuscar.TabIndex = 10;
            this.btnBuscar.Text = "Buscar por nombre";
            this.btnBuscar.UseVisualStyleBackColor = true;
            this.btnBuscar.Click += new System.EventHandler(this.btnBuscar_Click);
            // 
            // CRUD_Libro
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::LibraryAdministrator.Properties.Resources.music_background_3;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btnBuscar);
            this.Controls.Add(this.btn_Borrar);
            this.Controls.Add(this.btn_Actualizar);
            this.Controls.Add(this.btn_Crear);
            this.Controls.Add(this.txt_Nombre);
            this.Controls.Add(this.DGV_Libro);
            this.Controls.Add(this.cmb_Editorial);
            this.Controls.Add(this.cmb_Autor);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lbl_Nombre);
            this.Name = "CRUD_Libro";
            this.Text = "CRUD_Libro";
            this.Load += new System.EventHandler(this.CRUD_Libro_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DGV_Libro)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_Nombre;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmb_Autor;
        private System.Windows.Forms.ComboBox cmb_Editorial;
        private System.Windows.Forms.DataGridView DGV_Libro;
        private System.Windows.Forms.TextBox txt_Nombre;
        private System.Windows.Forms.Button btn_Crear;
        private System.Windows.Forms.Button btn_Actualizar;
        private System.Windows.Forms.Button btn_Borrar;
        private System.Windows.Forms.Button btnBuscar;
    }
}