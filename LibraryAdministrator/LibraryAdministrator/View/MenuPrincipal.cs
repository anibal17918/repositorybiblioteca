﻿using LibraryAdministrator.Core;
using LibraryAdministrator.DBModel.entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LibraryAdministrator.View
{
    public partial class MenuPrincipal : Form
    {
        GenericRepository<Autor> _autorRepository = new GenericRepository<Autor>();
        
        public MenuPrincipal()
        {
            InitializeComponent();
        }

        private void MenuPrincipal_Load(object sender, EventArgs e)
        {

        }

        private void btn_Autor_Click(object sender, EventArgs e)
        {
            CRUD_AUTOR crud_autor = new CRUD_AUTOR();
            crud_autor.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            CRUD_ESTUDIANTE estudiante = new CRUD_ESTUDIANTE();
            estudiante.ShowDialog();

        }

        private void btn_Editorial_Click(object sender, EventArgs e)
        {
            CRUD_EDITORIAL crud_editorial = new CRUD_EDITORIAL();
            crud_editorial.Show();
        }

        private void btnLibros_Click(object sender, EventArgs e)
        {
            CRUD_Libro libro = new CRUD_Libro();
            libro.ShowDialog();
        }

        private void btn_Prestamo_Click(object sender, EventArgs e)
        {
            CRUD_PRESTAMO prestamo = new CRUD_PRESTAMO();
            prestamo.ShowDialog();
        }
    }
}
