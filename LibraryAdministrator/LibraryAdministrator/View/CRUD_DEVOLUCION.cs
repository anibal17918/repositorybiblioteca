﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LibraryAdministrator.DBModel.entities;
using LibraryAdministrator.Repository;
using LibraryAdministrator.Interfaces;

namespace LibraryAdministrator.View
{
    public partial class CRUD_DEVOLUCION : Form
    {
        IDevolucionRepository _Devolucion = new DevolucionRepository();
        IPrestamoRepository _prestamo = new PrestamoRepository();
        ILibroRepository _Libro = new LibroRepository();
        IEstudianteRepository _estudiante = new EstudianteRepository();
        public CRUD_DEVOLUCION()
        {
            InitializeComponent();
        }

        public void Cargar()
        {
            DGVDevolucion.DataSource = _Devolucion.GetAll().Where(p => p.Estatus== "d").
                Select(p => new { p.id, p.FechaRegistro, p.FechaModificacion }).ToList();
            DGVPrestamos.DataSource = _prestamo.GetAll().Where(p => p.Estatus == "p").Select(p => new { p.id, Estudiante = _estudiante.FindbyID(p.Estudianteid).Nombre, Libro = _Libro.FindbyID(p.Libroid).Nombre, p.Fecha_Entrega, p.FechaRegistro, p.FechaModificacion }).ToList();
        }

        private void CRUD_DEVOLUCION_Load(object sender, EventArgs e)
        {
            Cargar();

        }

        private void btnDevolver_Click(object sender, EventArgs e)
        {
            int prestamoid = (int)DGVPrestamos.CurrentRow.Cells[0].Value;
            Prestamo prestamo = _prestamo.FindbyID(prestamoid);
            prestamo.Estatus = "d";
            Devolucion devolucion = new Devolucion();
            devolucion.Estatus = "d";
            devolucion.Prestamoid = prestamoid;
            devolucion.FechaModificacion = DateTime.Today;
            devolucion.FechaRegistro = DateTime.Today;
            
            devolucion.borrado = false;

            if (prestamo.id < 0)
            {
                MessageBox.Show("Debe indicar un prestamo valido");
            }
            else
            {
                if (MessageBox.Show("Desea modificar este prestamo", "Modificar", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    var borrar = _prestamo.Update(prestamo);
                    _Devolucion.Create(devolucion);

                    Cargar();

                }
                else
                {
                    MessageBox.Show("No se realizo ninguna accion");


                }
            }
        }
    }
}
