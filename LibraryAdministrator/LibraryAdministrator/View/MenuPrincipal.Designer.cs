﻿namespace LibraryAdministrator.View
{
    partial class MenuPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MenuPrincipal));
            this.btn_Autor = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.btn_Editorial = new System.Windows.Forms.Button();
            this.btnLibros = new System.Windows.Forms.Button();
            this.btn_Prestamo = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btn_Autor
            // 
            this.btn_Autor.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_Autor.BackgroundImage")));
            this.btn_Autor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Autor.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Autor.ForeColor = System.Drawing.SystemColors.Control;
            this.btn_Autor.Location = new System.Drawing.Point(31, 115);
            this.btn_Autor.Name = "btn_Autor";
            this.btn_Autor.Size = new System.Drawing.Size(118, 55);
            this.btn_Autor.TabIndex = 0;
            this.btn_Autor.Text = "Autor";
            this.btn_Autor.UseVisualStyleBackColor = true;
            this.btn_Autor.Click += new System.EventHandler(this.btn_Autor_Click);
            // 
            // button2
            // 
            this.button2.BackgroundImage = global::LibraryAdministrator.Properties.Resources.music_background_3;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ForeColor = System.Drawing.SystemColors.Control;
            this.button2.Location = new System.Drawing.Point(495, 115);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(114, 55);
            this.button2.TabIndex = 1;
            this.button2.Text = "Estudiantes";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // btn_Editorial
            // 
            this.btn_Editorial.BackgroundImage = global::LibraryAdministrator.Properties.Resources.music_placeholder_album6_50;
            this.btn_Editorial.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Editorial.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Editorial.ForeColor = System.Drawing.SystemColors.Control;
            this.btn_Editorial.Location = new System.Drawing.Point(184, 115);
            this.btn_Editorial.Name = "btn_Editorial";
            this.btn_Editorial.Size = new System.Drawing.Size(114, 55);
            this.btn_Editorial.TabIndex = 2;
            this.btn_Editorial.Text = "Editorial";
            this.btn_Editorial.UseVisualStyleBackColor = true;
            this.btn_Editorial.Click += new System.EventHandler(this.btn_Editorial_Click);
            // 
            // btnLibros
            // 
            this.btnLibros.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnLibros.BackgroundImage")));
            this.btnLibros.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLibros.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLibros.ForeColor = System.Drawing.SystemColors.Control;
            this.btnLibros.Location = new System.Drawing.Point(342, 115);
            this.btnLibros.Name = "btnLibros";
            this.btnLibros.Size = new System.Drawing.Size(114, 55);
            this.btnLibros.TabIndex = 3;
            this.btnLibros.Text = "Libros";
            this.btnLibros.UseVisualStyleBackColor = true;
            this.btnLibros.Click += new System.EventHandler(this.btnLibros_Click);
            // 
            // btn_Prestamo
            // 
            this.btn_Prestamo.BackgroundImage = global::LibraryAdministrator.Properties.Resources.music_background_7;
            this.btn_Prestamo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Prestamo.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Prestamo.ForeColor = System.Drawing.SystemColors.Control;
            this.btn_Prestamo.Location = new System.Drawing.Point(651, 113);
            this.btn_Prestamo.Name = "btn_Prestamo";
            this.btn_Prestamo.Size = new System.Drawing.Size(114, 59);
            this.btn_Prestamo.TabIndex = 4;
            this.btn_Prestamo.Text = "Prestamo";
            this.btn_Prestamo.UseVisualStyleBackColor = true;
            this.btn_Prestamo.Click += new System.EventHandler(this.btn_Prestamo_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.Control;
            this.label1.Location = new System.Drawing.Point(319, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(152, 33);
            this.label1.TabIndex = 5;
            this.label1.Text = "Biblioteca";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.Control;
            this.label2.Location = new System.Drawing.Point(28, 260);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(526, 18);
            this.label2.TabIndex = 6;
            this.label2.Text = "Los campos ID solo es necesario llenarlo cuando se va a actualizar / modificar";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.Control;
            this.label3.Location = new System.Drawing.Point(28, 233);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(96, 18);
            this.label3.TabIndex = 7;
            this.label3.Text = "Instrucciones";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.Control;
            this.label4.Location = new System.Drawing.Point(28, 292);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(278, 18);
            this.label4.TabIndex = 8;
            this.label4.Text = "Si es su primera vez usando el programa";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.Control;
            this.label5.Location = new System.Drawing.Point(28, 319);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(474, 18);
            this.label5.TabIndex = 9;
            this.label5.Text = "Seguir el orden: Autor -> Editorial -> Libros -> Estudiantes -> Prestamo";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.SystemColors.Control;
            this.label6.Location = new System.Drawing.Point(349, 42);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(80, 18);
            this.label6.TabIndex = 10;
            this.label6.Text = "Bienvenido";
            // 
            // MenuPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::LibraryAdministrator.Properties.Resources.music_background_3;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btn_Prestamo);
            this.Controls.Add(this.btnLibros);
            this.Controls.Add(this.btn_Editorial);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.btn_Autor);
            this.Name = "MenuPrincipal";
            this.Text = "MenuPrincipal";
            this.Load += new System.EventHandler(this.MenuPrincipal_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_Autor;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button btn_Editorial;
        private System.Windows.Forms.Button btnLibros;
        private System.Windows.Forms.Button btn_Prestamo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
    }
}