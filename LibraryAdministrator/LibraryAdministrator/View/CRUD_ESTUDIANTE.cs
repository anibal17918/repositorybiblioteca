﻿using LibraryAdministrator.Core;
using LibraryAdministrator.DBModel.entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LibraryAdministrator.Interfaces;
using LibraryAdministrator.Repository;

namespace LibraryAdministrator.View
{
    public partial class CRUD_ESTUDIANTE : Form
    {
        IEstudianteRepository _Estudiante = new EstudianteRepository();
        Estudiante Estudiante = new Estudiante();
        public CRUD_ESTUDIANTE()
        {
            InitializeComponent();
        }

        private void CRUD_ESTUDIANTE_Load(object sender, EventArgs e)
        {
            Cargar();
            

        }
        public void Cargar() { DgvEstudiante.DataSource = _Estudiante.GetAll().Select(p => new { p.id, p.Nombre, p.Apellido, p.Telefono, p.Direccion, p.Estatus ,p.FechaRegistro, p.FechaModificacion }).ToList(); }

        private void button1_Click(object sender, EventArgs e)
        {
            _Estudiante.Create(new Estudiante()
            {
                Nombre = txtNombreEstudiante.Text,
                Apellido = txtApellidoEstudiante.Text,
                Direccion= txtDireccionEstudiante.Text,
                Telefono = txtTelefonoEstudiante.Text,
                borrado = false,
                Estatus = txtEstatusEstudiante.Text,
                FechaRegistro = DateTime.Today,
                FechaModificacion = DateTime.Today
            });

            MessageBox.Show("Fue Creado Exitosamente");
            Cargar();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Estudiante estudiante = new Estudiante();
            int idEstudiante = (int)DgvEstudiante.CurrentRow.Cells[0].Value;
            estudiante = _Estudiante.FindbyID(idEstudiante);
            if (idEstudiante < 0)
            {
                MessageBox.Show("Debe indicar un suplidor valido");
            }
            else
            {
                if (MessageBox.Show("Desea borrar este empleado", "Borrar", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    var borrarsuplidor = _Estudiante.Delete(estudiante);
                    Cargar();

                }
                else
                {
                    MessageBox.Show("No se realizo ninguna accion");

                }
            }


        }

        private void btnActulizarEstudiante_Click(object sender, EventArgs e)
        {
            int estudianteid = (int)DgvEstudiante.CurrentRow.Cells[0].Value;
            Estudiante = _Estudiante.FindbyID(estudianteid);

            Estudiante.Nombre = txtNombreEstudiante.Text;
            Estudiante.Apellido = txtApellidoEstudiante.Text;
            Estudiante.Direccion = txtDireccionEstudiante.Text;
            Estudiante.Telefono = txtTelefonoEstudiante.Text;
            Estudiante.Estatus = txtEstatusEstudiante.Text;
            Estudiante.FechaModificacion = DateTime.Today;

            Estudiante.id = (int)DgvEstudiante.CurrentRow.Cells[0].Value;
            if (Estudiante.id < 0)
            {
                MessageBox.Show("Debe indicar un Estudiante valido");
            }
            else
            {
                if (MessageBox.Show("Desea modificar este estudiante", "Modificar", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    var borrar = _Estudiante.Update(Estudiante);
                    Cargar();

                }
                else
                {
                    MessageBox.Show("No se realizo ninguna accion");


                }
            }
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            string Telefono = txtTelefonoEstudiante.Text;
            DgvEstudiante.DataSource = _Estudiante.GetAll().Where(p => p.Telefono.Contains(Telefono)).Select(p => new { p.id, p.Nombre, p.Apellido, p.Telefono, p.Direccion, p.Estatus, p.FechaRegistro, p.FechaModificacion }).ToList();
        }
    }
}
