﻿
namespace LibraryAdministrator.View
{
    partial class CRUD_EDITORIAL
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_Nombre = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txt_Nombre = new System.Windows.Forms.TextBox();
            this.txt_Dirección = new System.Windows.Forms.TextBox();
            this.txt_Estatus = new System.Windows.Forms.TextBox();
            this.btn_Crear = new System.Windows.Forms.Button();
            this.btn_Borrar = new System.Windows.Forms.Button();
            this.btn_Actualizar = new System.Windows.Forms.Button();
            this.btn_refresh = new System.Windows.Forms.Button();
            this.DGV_Editorial = new System.Windows.Forms.DataGridView();
            this.label3 = new System.Windows.Forms.Label();
            this.txt_ID = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.DGV_Editorial)).BeginInit();
            this.SuspendLayout();
            // 
            // lbl_Nombre
            // 
            this.lbl_Nombre.AutoSize = true;
            this.lbl_Nombre.BackColor = System.Drawing.Color.Transparent;
            this.lbl_Nombre.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Nombre.Location = new System.Drawing.Point(-2, 53);
            this.lbl_Nombre.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbl_Nombre.Name = "lbl_Nombre";
            this.lbl_Nombre.Size = new System.Drawing.Size(63, 16);
            this.lbl_Nombre.TabIndex = 0;
            this.lbl_Nombre.Text = "Nombre";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(-2, 87);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 16);
            this.label1.TabIndex = 1;
            this.label1.Text = "Dirección";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(-2, 117);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 16);
            this.label2.TabIndex = 2;
            this.label2.Text = "Estatus";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // txt_Nombre
            // 
            this.txt_Nombre.Location = new System.Drawing.Point(76, 49);
            this.txt_Nombre.Margin = new System.Windows.Forms.Padding(2);
            this.txt_Nombre.Name = "txt_Nombre";
            this.txt_Nombre.Size = new System.Drawing.Size(206, 20);
            this.txt_Nombre.TabIndex = 3;
            this.txt_Nombre.TextChanged += new System.EventHandler(this.txt_Nombre_TextChanged);
            // 
            // txt_Dirección
            // 
            this.txt_Dirección.Location = new System.Drawing.Point(76, 83);
            this.txt_Dirección.Margin = new System.Windows.Forms.Padding(2);
            this.txt_Dirección.Name = "txt_Dirección";
            this.txt_Dirección.Size = new System.Drawing.Size(206, 20);
            this.txt_Dirección.TabIndex = 4;
            this.txt_Dirección.TextChanged += new System.EventHandler(this.txt_Dirección_TextChanged);
            // 
            // txt_Estatus
            // 
            this.txt_Estatus.Location = new System.Drawing.Point(76, 113);
            this.txt_Estatus.Margin = new System.Windows.Forms.Padding(2);
            this.txt_Estatus.Name = "txt_Estatus";
            this.txt_Estatus.Size = new System.Drawing.Size(206, 20);
            this.txt_Estatus.TabIndex = 5;
            // 
            // btn_Crear
            // 
            this.btn_Crear.BackColor = System.Drawing.Color.White;
            this.btn_Crear.BackgroundImage = global::LibraryAdministrator.Properties.Resources.music_background_8;
            this.btn_Crear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Crear.ForeColor = System.Drawing.Color.Black;
            this.btn_Crear.Location = new System.Drawing.Point(308, 19);
            this.btn_Crear.Margin = new System.Windows.Forms.Padding(2);
            this.btn_Crear.Name = "btn_Crear";
            this.btn_Crear.Size = new System.Drawing.Size(122, 50);
            this.btn_Crear.TabIndex = 6;
            this.btn_Crear.Text = "Crear";
            this.btn_Crear.UseVisualStyleBackColor = false;
            this.btn_Crear.Click += new System.EventHandler(this.btn_Crear_Click);
            // 
            // btn_Borrar
            // 
            this.btn_Borrar.BackgroundImage = global::LibraryAdministrator.Properties.Resources.music_background_8;
            this.btn_Borrar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Borrar.Location = new System.Drawing.Point(308, 83);
            this.btn_Borrar.Margin = new System.Windows.Forms.Padding(2);
            this.btn_Borrar.Name = "btn_Borrar";
            this.btn_Borrar.Size = new System.Drawing.Size(122, 50);
            this.btn_Borrar.TabIndex = 7;
            this.btn_Borrar.Text = "Borrar";
            this.btn_Borrar.UseVisualStyleBackColor = true;
            this.btn_Borrar.Click += new System.EventHandler(this.button1_Click);
            // 
            // btn_Actualizar
            // 
            this.btn_Actualizar.BackgroundImage = global::LibraryAdministrator.Properties.Resources.music_background_8;
            this.btn_Actualizar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Actualizar.Location = new System.Drawing.Point(473, 19);
            this.btn_Actualizar.Margin = new System.Windows.Forms.Padding(2);
            this.btn_Actualizar.Name = "btn_Actualizar";
            this.btn_Actualizar.Size = new System.Drawing.Size(125, 50);
            this.btn_Actualizar.TabIndex = 8;
            this.btn_Actualizar.Text = "Actualizar";
            this.btn_Actualizar.UseVisualStyleBackColor = true;
            this.btn_Actualizar.Click += new System.EventHandler(this.btn_Actualizar_Click);
            // 
            // btn_refresh
            // 
            this.btn_refresh.BackgroundImage = global::LibraryAdministrator.Properties.Resources.music_background_8;
            this.btn_refresh.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_refresh.Location = new System.Drawing.Point(473, 83);
            this.btn_refresh.Margin = new System.Windows.Forms.Padding(2);
            this.btn_refresh.Name = "btn_refresh";
            this.btn_refresh.Size = new System.Drawing.Size(125, 50);
            this.btn_refresh.TabIndex = 9;
            this.btn_refresh.Text = "Buscar Nombre";
            this.btn_refresh.UseVisualStyleBackColor = true;
            this.btn_refresh.Click += new System.EventHandler(this.btn_refresh_Click);
            // 
            // DGV_Editorial
            // 
            this.DGV_Editorial.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGV_Editorial.Location = new System.Drawing.Point(17, 148);
            this.DGV_Editorial.Margin = new System.Windows.Forms.Padding(2);
            this.DGV_Editorial.Name = "DGV_Editorial";
            this.DGV_Editorial.RowHeadersWidth = 51;
            this.DGV_Editorial.RowTemplate.Height = 24;
            this.DGV_Editorial.Size = new System.Drawing.Size(581, 189);
            this.DGV_Editorial.TabIndex = 10;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(2, 19);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(19, 16);
            this.label3.TabIndex = 11;
            this.label3.Text = "N";
            // 
            // txt_ID
            // 
            this.txt_ID.Location = new System.Drawing.Point(76, 15);
            this.txt_ID.Name = "txt_ID";
            this.txt_ID.Size = new System.Drawing.Size(39, 20);
            this.txt_ID.TabIndex = 12;
            this.txt_ID.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // CRUD_EDITORIAL
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::LibraryAdministrator.Properties.Resources.music_background_8;
            this.ClientSize = new System.Drawing.Size(609, 366);
            this.Controls.Add(this.txt_ID);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.DGV_Editorial);
            this.Controls.Add(this.btn_refresh);
            this.Controls.Add(this.btn_Actualizar);
            this.Controls.Add(this.btn_Borrar);
            this.Controls.Add(this.btn_Crear);
            this.Controls.Add(this.txt_Estatus);
            this.Controls.Add(this.txt_Dirección);
            this.Controls.Add(this.txt_Nombre);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lbl_Nombre);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "CRUD_EDITORIAL";
            this.Text = "CRUD_EDITORIAL";
            this.Load += new System.EventHandler(this.CRUD_EDITORIAL_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DGV_Editorial)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_Nombre;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txt_Nombre;
        private System.Windows.Forms.TextBox txt_Dirección;
        private System.Windows.Forms.TextBox txt_Estatus;
        private System.Windows.Forms.Button btn_Crear;
        private System.Windows.Forms.Button btn_Borrar;
        private System.Windows.Forms.Button btn_Actualizar;
        private System.Windows.Forms.Button btn_refresh;
        private System.Windows.Forms.DataGridView DGV_Editorial;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txt_ID;
    }
}