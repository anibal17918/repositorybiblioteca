﻿using LibraryAdministrator.DBModel.entities;
using LibraryAdministrator.Interfaces;
using LibraryAdministrator.Repository;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LibraryAdministrator.View
{
    public partial class CRUD_EDITORIAL : Form
    {
        IEditorialRepository _editorialRepository = new EditorialRepository();
        
        public CRUD_EDITORIAL()
        {
            InitializeComponent();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void btn_Crear_Click(object sender, EventArgs e)
        {
            try
            {
                _editorialRepository.Create(new Editorial()
                {
                    Nombre = txt_Nombre.Text,
                    Direccion = txt_Dirección.Text,
                    borrado = false,
                    Estatus = txt_Estatus.Text,
                    FechaRegistro = DateTime.Today,
                    FechaModificacion = DateTime.Today
                });
                MessageBox.Show("Fue Creado Exitosamente");
            }
            catch (Exception)
            {
                MessageBox.Show("ha ocurrido un error");
                throw;
            }
        }

        private void txt_Nombre_TextChanged(object sender, EventArgs e)
        {

        }
        private void cargar()
        {
            DGV_Editorial.DataSource = _editorialRepository.GetAll().Select(p => new { p.id, p.Nombre, p.Direccion, p.FechaRegistro, p.FechaModificacion }).ToList();

        }
        private void CRUD_EDITORIAL_Load(object sender, EventArgs e)
        {
            DGV_Editorial.DataSource = _editorialRepository.GetAll().Select(p => new { p.id, p.Nombre, p.Direccion, p.FechaRegistro, p.FechaModificacion }).ToList();

        }

        private void btn_refresh_Click(object sender, EventArgs e)

        {
            string Nombre = txt_Nombre.Text;
            DGV_Editorial.DataSource = _editorialRepository.GetAll().Where(p => p.Nombre.Contains(Nombre)).Select(p => new { p.id, p.Nombre, p.Direccion, p.FechaRegistro, p.FechaModificacion }).ToList();
            //try
            //{
            //    int a;
            //    bool isNumeric = int.TryParse(txt_ID.Text, out a);
            //    if (isNumeric)
            //    {
            //        //para saber si es un numero (int)
            //    }
            //    else 
            //    {
            //        MessageBox.Show("Inserte un ID valido");
            //    }

            //    try
            //    {
            //        Editorial editorial = _editorialRepository.FindbyID(int.Parse(txt_ID.Text));

            //        foreach (DataGridViewRow row in DGV_Editorial.Rows)
            //        {
            //            if (row.Cells[0].Value.ToString().Contains(txt_ID.Text))
            //            {
            //                DGV_Editorial.DataSource = _editorialRepository.GetAll().Where(p => p.id == editorial.id)
            //                    .Select(p => new { p.id, p.Nombre, p.Direccion, p.FechaRegistro, p.FechaModificacion }).ToList();
            //            }
            //            else 
            //            {
            //                cargar();
            //            }
            //        }
            //    }
            //    catch (Exception ex)
            //    {
            //        MessageBox.Show("ID no encontrado"+ ex.ToString());

            //    }
            //}
            //catch (FormatException ex)
            //{
            //    MessageBox.Show("Something went wrong " + ex.ToString());
            //}


        }
        
        
        //Boton de Borrar
        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                _editorialRepository.Delete(_editorialRepository.FindbyID(int.Parse(txt_ID.Text)));
                MessageBox.Show("Fue Borrado Exitosamente");
            }
            catch (Exception)
            {
                MessageBox.Show("Inserte el id, solo incluye números / El id no fue encontrado");
            }
        }

        private void txt_Dirección_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void btn_Actualizar_Click(object sender, EventArgs e)
        {
            try
            {
                Editorial editorial = _editorialRepository.FindbyID(int.Parse(txt_ID.Text));

                editorial.Nombre = txt_Nombre.Text;
                editorial.Direccion = txt_Dirección.Text;
                editorial.Estatus = txt_Estatus.Text;
                editorial.FechaModificacion = DateTime.Today;


                _editorialRepository.Update(editorial);
                MessageBox.Show("fue actualizado exitosamente");
            }
            catch (Exception)
            {
                MessageBox.Show("inserte el id solo incluye números / el id no fue encontrado");
            }
        }
    }
}
