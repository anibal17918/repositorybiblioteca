﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LibraryAdministrator.DBModel.entities;
using LibraryAdministrator.Repository;
using LibraryAdministrator.Interfaces;

namespace LibraryAdministrator.View
{
    public partial class CRUD_Libro : Form
    {
        ILibroRepository _Libro = new LibroRepository();
        Autor Autor = new Autor();
        Libro Libro = new Libro();
        IAutorRepository _Autor = new AutorRepository();
        IEditorialRepository _Editorial = new EditorialRepository();
        
        
        public CRUD_Libro()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        public void Cargar()
        {
            DGV_Libro.DataSource = _Libro.GetAll().Select(p => new { p.id, p.Nombre, Autor = _Autor.FindbyID(p.Autorid).Nombre , Editorial = _Editorial.FindbyID(p.Editorialid).Nombre}).ToList();
            cmb_Editorial.DataSource = _Editorial.GetAll().Select(p => new { p.Nombre, p.id }).ToList();
            cmb_Editorial.DisplayMember = "Nombre";
            cmb_Editorial.ValueMember = "id";
            cmb_Autor.DataSource = _Autor.GetAll().Select(p => new { p.Nombre, p.id }).ToList();
            cmb_Autor.DisplayMember = "Nombre";
            cmb_Autor.ValueMember = "id";
        }

        private void cmb_Autor_SelectedIndexChanged(object sender, EventArgs e)
        {
            

        }

        private void CRUD_Libro_Load(object sender, EventArgs e)
        {
           

            Cargar();
        }


        private void cmb_Editorial_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void btn_Crear_Click(object sender, EventArgs e)
        {
            Libro.Nombre = txt_Nombre.Text;
            Libro.Estatus = "Ac";
            Libro.Autorid = (int)cmb_Autor.SelectedValue;
            Libro.Editorialid = (int)cmb_Editorial.SelectedValue; 
            Libro.FechaModificacion = DateTime.Today;
            Libro.FechaRegistro = DateTime.Today;
            _Libro.Create(Libro);
            MessageBox.Show("Datos guardados correctamente");
            Cargar();
            
        }

        private void btn_Borrar_Click(object sender, EventArgs e)
        {
            int libroid = (int)DGV_Libro.CurrentRow.Cells[0].Value;
            Libro = _Libro.FindbyID(libroid);
            if (libroid < 0)
            {
                MessageBox.Show("Debe indicar un Libro valido");
            }
            else
            {
                if (MessageBox.Show("Desea borrar este Libro", "Borrar", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    var borrar = _Libro.Delete(Libro);
                    Cargar();

                }
                else
                {
                    MessageBox.Show("No se realizo ninguna accion");
                    

                }
            }
        }

        private void btn_Actualizar_Click(object sender, EventArgs e)
        {
            Libro.Nombre = txt_Nombre.Text;
            Libro.Autorid = (int)cmb_Autor.SelectedValue;
            Libro.Editorialid = (int)cmb_Editorial.SelectedValue;
            Libro.FechaModificacion = DateTime.Today;
            Libro.Estatus = "Ac";
            Libro.id = (int)DGV_Libro.CurrentRow.Cells[0].Value;
            
            if (Libro.id < 0)
            {
                MessageBox.Show("Debe indicar un Libro valido");
            }
            else
            {
                if (MessageBox.Show("Desea actulizar este Libro", "Actualizar", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    var borrar = _Libro.Update(Libro);
                    Cargar();

                }
                else
                {
                    MessageBox.Show("No se realizo ninguna accion");


                }
            }
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            string Nombre = txt_Nombre.Text;
            DGV_Libro.DataSource = _Libro.GetAll().Where(p => p.Nombre.Contains(Nombre)).
                Select(p => new { p.id, p.Nombre, Autor = _Autor.FindbyID(p.Autorid).Nombre, Editorial = _Editorial.FindbyID(p.Editorialid).Nombre }).ToList();
        }
    }
}
