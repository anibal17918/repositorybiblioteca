﻿
namespace LibraryAdministrator.View
{
    partial class CRUD_ESTUDIANTE
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtNombreEstudiante = new System.Windows.Forms.TextBox();
            this.txtApellidoEstudiante = new System.Windows.Forms.TextBox();
            this.txtDireccionEstudiante = new System.Windows.Forms.TextBox();
            this.txtTelefonoEstudiante = new System.Windows.Forms.TextBox();
            this.txtEstatusEstudiante = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.btnBorrarEstudiante = new System.Windows.Forms.Button();
            this.btnActulizarEstudiante = new System.Windows.Forms.Button();
            this.DgvEstudiante = new System.Windows.Forms.DataGridView();
            this.btnBuscar = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.DgvEstudiante)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.Control;
            this.label1.Location = new System.Drawing.Point(14, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nombre";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.Control;
            this.label2.Location = new System.Drawing.Point(12, 51);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 15);
            this.label2.TabIndex = 1;
            this.label2.Text = "Apellido";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.Control;
            this.label3.Location = new System.Drawing.Point(12, 77);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 15);
            this.label3.TabIndex = 2;
            this.label3.Text = "Direccion";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.Control;
            this.label4.Location = new System.Drawing.Point(14, 103);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(63, 15);
            this.label4.TabIndex = 3;
            this.label4.Text = "Telefono";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.Control;
            this.label5.Location = new System.Drawing.Point(14, 129);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(54, 15);
            this.label5.TabIndex = 4;
            this.label5.Text = "Estatus";
            // 
            // txtNombreEstudiante
            // 
            this.txtNombreEstudiante.Location = new System.Drawing.Point(87, 22);
            this.txtNombreEstudiante.Name = "txtNombreEstudiante";
            this.txtNombreEstudiante.Size = new System.Drawing.Size(249, 20);
            this.txtNombreEstudiante.TabIndex = 5;
            // 
            // txtApellidoEstudiante
            // 
            this.txtApellidoEstudiante.Location = new System.Drawing.Point(87, 48);
            this.txtApellidoEstudiante.Name = "txtApellidoEstudiante";
            this.txtApellidoEstudiante.Size = new System.Drawing.Size(249, 20);
            this.txtApellidoEstudiante.TabIndex = 6;
            // 
            // txtDireccionEstudiante
            // 
            this.txtDireccionEstudiante.Location = new System.Drawing.Point(87, 74);
            this.txtDireccionEstudiante.Name = "txtDireccionEstudiante";
            this.txtDireccionEstudiante.Size = new System.Drawing.Size(249, 20);
            this.txtDireccionEstudiante.TabIndex = 7;
            // 
            // txtTelefonoEstudiante
            // 
            this.txtTelefonoEstudiante.Location = new System.Drawing.Point(87, 102);
            this.txtTelefonoEstudiante.Name = "txtTelefonoEstudiante";
            this.txtTelefonoEstudiante.Size = new System.Drawing.Size(249, 20);
            this.txtTelefonoEstudiante.TabIndex = 8;
            // 
            // txtEstatusEstudiante
            // 
            this.txtEstatusEstudiante.Location = new System.Drawing.Point(87, 128);
            this.txtEstatusEstudiante.Name = "txtEstatusEstudiante";
            this.txtEstatusEstudiante.Size = new System.Drawing.Size(46, 20);
            this.txtEstatusEstudiante.TabIndex = 9;
            // 
            // button1
            // 
            this.button1.BackgroundImage = global::LibraryAdministrator.Properties.Resources.music_background_8;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.SystemColors.Control;
            this.button1.Location = new System.Drawing.Point(423, 22);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(100, 49);
            this.button1.TabIndex = 10;
            this.button1.Text = "Crear";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnBorrarEstudiante
            // 
            this.btnBorrarEstudiante.BackgroundImage = global::LibraryAdministrator.Properties.Resources.music_background_7;
            this.btnBorrarEstudiante.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBorrarEstudiante.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBorrarEstudiante.ForeColor = System.Drawing.SystemColors.Control;
            this.btnBorrarEstudiante.Location = new System.Drawing.Point(596, 22);
            this.btnBorrarEstudiante.Name = "btnBorrarEstudiante";
            this.btnBorrarEstudiante.Size = new System.Drawing.Size(100, 49);
            this.btnBorrarEstudiante.TabIndex = 11;
            this.btnBorrarEstudiante.Text = "Borrar";
            this.btnBorrarEstudiante.UseVisualStyleBackColor = true;
            this.btnBorrarEstudiante.Click += new System.EventHandler(this.button2_Click);
            // 
            // btnActulizarEstudiante
            // 
            this.btnActulizarEstudiante.BackgroundImage = global::LibraryAdministrator.Properties.Resources.music_placeholder_album6_50;
            this.btnActulizarEstudiante.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnActulizarEstudiante.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnActulizarEstudiante.ForeColor = System.Drawing.SystemColors.Control;
            this.btnActulizarEstudiante.Location = new System.Drawing.Point(508, 93);
            this.btnActulizarEstudiante.Name = "btnActulizarEstudiante";
            this.btnActulizarEstudiante.Size = new System.Drawing.Size(100, 49);
            this.btnActulizarEstudiante.TabIndex = 12;
            this.btnActulizarEstudiante.Text = "Actualizar";
            this.btnActulizarEstudiante.UseVisualStyleBackColor = true;
            this.btnActulizarEstudiante.Click += new System.EventHandler(this.btnActulizarEstudiante_Click);
            // 
            // DgvEstudiante
            // 
            this.DgvEstudiante.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DgvEstudiante.Location = new System.Drawing.Point(17, 163);
            this.DgvEstudiante.Name = "DgvEstudiante";
            this.DgvEstudiante.Size = new System.Drawing.Size(679, 212);
            this.DgvEstudiante.TabIndex = 13;
            // 
            // btnBuscar
            // 
            this.btnBuscar.BackgroundImage = global::LibraryAdministrator.Properties.Resources.music_placeholder_album6_50;
            this.btnBuscar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBuscar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBuscar.ForeColor = System.Drawing.SystemColors.Control;
            this.btnBuscar.Location = new System.Drawing.Point(370, 95);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(100, 49);
            this.btnBuscar.TabIndex = 14;
            this.btnBuscar.Text = "Buscar por Telefono";
            this.btnBuscar.UseVisualStyleBackColor = true;
            this.btnBuscar.Click += new System.EventHandler(this.btnBuscar_Click);
            // 
            // CRUD_ESTUDIANTE
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::LibraryAdministrator.Properties.Resources.music_background_7;
            this.ClientSize = new System.Drawing.Size(708, 387);
            this.Controls.Add(this.btnBuscar);
            this.Controls.Add(this.DgvEstudiante);
            this.Controls.Add(this.btnActulizarEstudiante);
            this.Controls.Add(this.btnBorrarEstudiante);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.txtEstatusEstudiante);
            this.Controls.Add(this.txtTelefonoEstudiante);
            this.Controls.Add(this.txtDireccionEstudiante);
            this.Controls.Add(this.txtApellidoEstudiante);
            this.Controls.Add(this.txtNombreEstudiante);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "CRUD_ESTUDIANTE";
            this.Text = "CRUD_ESTUDIANTE";
            this.Load += new System.EventHandler(this.CRUD_ESTUDIANTE_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DgvEstudiante)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtNombreEstudiante;
        private System.Windows.Forms.TextBox txtApellidoEstudiante;
        private System.Windows.Forms.TextBox txtDireccionEstudiante;
        private System.Windows.Forms.TextBox txtTelefonoEstudiante;
        private System.Windows.Forms.TextBox txtEstatusEstudiante;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnBorrarEstudiante;
        private System.Windows.Forms.Button btnActulizarEstudiante;
        private System.Windows.Forms.DataGridView DgvEstudiante;
        private System.Windows.Forms.Button btnBuscar;
    }
}