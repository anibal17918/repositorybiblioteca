﻿using LibraryAdministrator.DBModel.entities;
using LibraryAdministrator.Interfaces;
using LibraryAdministrator.Repository;
using LibraryAdministrator.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LibraryAdministrator.View
{
    public partial class CRUD_PRESTAMO : Form
    {
        IDevolucionRepository _Devolucion = new DevolucionRepository();
        Devolucion Devoluciones = new Devolucion();
        
        ILibroRepository _Libro = new LibroRepository();
        Libro Libro = new Libro();
        IEstudianteRepository _estudiante = new EstudianteRepository();
        Estudiante estudiante = new Estudiante();
        IAutorRepository _Autor = new AutorRepository();
        IEditorialRepository _Editorial = new EditorialRepository();
        IPrestamoRepository _prestamo = new PrestamoRepository();
        Prestamo prestamo = new Prestamo();

        public CRUD_PRESTAMO()
        {
            InitializeComponent();
            
        }

        private void CRUD_PRESTAMO_Load(object sender, EventArgs e)
        {
            cargar();
        }

        private void cargar() 
        {
            dgv_Prestamo.DataSource = _prestamo.GetAll().Where(p => p.Estatus == "p").Select(p => new { p.id, Estudiante = _estudiante.FindbyID(p.Estudianteid).Nombre, Libro = _Libro.FindbyID(p.Libroid).Nombre, Telefono = _estudiante.FindbyID(p.Estudianteid).Telefono, p.Fecha_Entrega, p.FechaRegistro, p.FechaModificacion }).ToList();
            cmb_Libro.DataSource = _Libro.GetAll().Select(p => new { p.Nombre, p.id }).ToList();
            cmb_Libro.DisplayMember = "Nombre";
            cmb_Libro.ValueMember = "id";
            cmb_Estudiante.DataSource = _estudiante.GetAll().Select(p => new { p.Nombre, p.id }).ToList();
            cmb_Estudiante.DisplayMember = "Nombre";
            cmb_Estudiante.ValueMember = "id";
        }

        private void cmb_Libro_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void cmb_Estudiante_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        //btn_Crear
        private void button1_Click(object sender, EventArgs e)
        {
            prestamo.Estudianteid = (int)cmb_Estudiante.SelectedValue;
            prestamo.Libroid = (int)cmb_Libro.SelectedValue;
            prestamo.Fecha_Entrega = date.Value;
            prestamo.FechaModificacion = DateTime.Today;
            prestamo.FechaRegistro = DateTime.Today;
            prestamo.Estatus = "p";
            var prestamoss =_prestamo.Create(prestamo);
            
            MessageBox.Show("Datos guardados correctamente");
            cargar();
        }

        private void btn_Modificar_Click(object sender, EventArgs e)
        {
            int prestamoid = (int)dgv_Prestamo.CurrentRow.Cells[0].Value;
            prestamo = _prestamo.FindbyID(prestamoid);

            prestamo.Estudianteid = (int)cmb_Estudiante.SelectedValue;
            prestamo.Libroid = (int)cmb_Libro.SelectedValue;
            prestamo.Fecha_Entrega = date.Value;
            prestamo.FechaModificacion = DateTime.Today;
            prestamo.id = (int)dgv_Prestamo.CurrentRow.Cells[0].Value;
            if (prestamo.id < 0)
            {
                MessageBox.Show("Debe indicar un prestamo valido");
            }
            else
            {
                if (MessageBox.Show("Desea modificar este prestamo", "Modificar", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    var borrar = _prestamo.Update(prestamo);
                    
                    cargar();

                }
                else
                {
                    MessageBox.Show("No se realizo ninguna accion");


                }
            }
        }

        private void btn_Borrar_Click(object sender, EventArgs e)
        {
            int prestamoid = (int)dgv_Prestamo.CurrentRow.Cells[0].Value;
                     
            prestamo = _prestamo.FindbyID(prestamoid);
           
            if (prestamoid < 0)
            {
                MessageBox.Show("Debe indicar un prestamo valido");
            }
            else
            {
                if (MessageBox.Show("Desea borrar este prestamo", "Borrar", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    var borrar = _prestamo.Delete(prestamo);
                   
                    cargar();

                }
                else
                {
                    MessageBox.Show("No se realizo ninguna accion");


                }
            }
        }

        private void btnDevoluciones_Click(object sender, EventArgs e)
        {
            CRUD_DEVOLUCION Debo = new CRUD_DEVOLUCION();
            Debo.ShowDialog();
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                string buscar = txtBuscar.Text;
                Estudiante estudiante = _estudiante.GetAll().Where(p => p.Telefono.Contains(buscar)).FirstOrDefault();

                dgv_Prestamo.DataSource = _prestamo.GetAll().Where(p => p.Estudianteid == estudiante.id).
                Select(p => new { p.id, Estudiante = _estudiante.FindbyID(p.Estudianteid).Nombre, Libro = _Libro.FindbyID(p.Libroid).Nombre, Telefono = estudiante.Telefono, p.Fecha_Entrega, p.FechaRegistro, p.FechaModificacion }).ToList(); ;

            }
            catch (FormatException ex)
            {
                MessageBox.Show("Something went wrong " + ex.ToString());
            }



        }
    }
}
