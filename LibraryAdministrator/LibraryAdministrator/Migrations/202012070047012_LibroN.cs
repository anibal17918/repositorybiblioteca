﻿namespace LibraryAdministrator.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class LibroN : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Prestamo", "Fecha_Entrega", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Prestamo", "Fecha_Entrega");
        }
    }
}
