﻿namespace LibraryAdministrator.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Inicial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Autor",
                c => new
                    {
                        Autorid = c.Int(nullable: false, identity: true),
                        Nombre = c.String(maxLength: 100, unicode: false),
                        Apellido = c.String(maxLength: 100, unicode: false),
                        Nacionalidad = c.String(maxLength: 100, unicode: false),
                        borrado = c.Boolean(nullable: false),
                        Estatus = c.String(maxLength: 2, unicode: false),
                        FechaRegistro = c.DateTime(nullable: false, storeType: "date"),
                        FechaModificacion = c.DateTime(nullable: false, storeType: "date"),
                    })
                .PrimaryKey(t => t.Autorid);
            
            CreateTable(
                "dbo.Libro",
                c => new
                    {
                        Libroid = c.Int(nullable: false, identity: true),
                        Nombre = c.String(maxLength: 200, unicode: false),
                        Autorid = c.Int(nullable: false),
                        Editorialid = c.Int(nullable: false),
                        borrado = c.Boolean(nullable: false),
                        Estatus = c.String(maxLength: 2, unicode: false),
                        FechaRegistro = c.DateTime(nullable: false, storeType: "date"),
                        FechaModificacion = c.DateTime(nullable: false, storeType: "date"),
                    })
                .PrimaryKey(t => t.Libroid)
                .ForeignKey("dbo.Editorial", t => t.Editorialid, cascadeDelete: true)
                .ForeignKey("dbo.Autor", t => t.Autorid, cascadeDelete: true)
                .Index(t => t.Autorid)
                .Index(t => t.Editorialid);
            
            CreateTable(
                "dbo.Editorial",
                c => new
                    {
                        Editorialid = c.Int(nullable: false, identity: true),
                        Nombre = c.String(maxLength: 500, unicode: false),
                        Direccion = c.String(maxLength: 500, unicode: false),
                        borrado = c.Boolean(nullable: false),
                        Estatus = c.String(maxLength: 2, unicode: false),
                        FechaRegistro = c.DateTime(nullable: false, storeType: "date"),
                        FechaModificacion = c.DateTime(nullable: false, storeType: "date"),
                    })
                .PrimaryKey(t => t.Editorialid);
            
            CreateTable(
                "dbo.Prestamo",
                c => new
                    {
                        Prestamoid = c.Int(nullable: false, identity: true),
                        Estudianteid = c.Int(nullable: false),
                        Libroid = c.Int(nullable: false),
                        borrado = c.Boolean(nullable: false),
                        Estatus = c.String(maxLength: 2, unicode: false),
                        FechaRegistro = c.DateTime(nullable: false, storeType: "date"),
                        FechaModificacion = c.DateTime(nullable: false, storeType: "date"),
                    })
                .PrimaryKey(t => t.Prestamoid)
                .ForeignKey("dbo.Estudiante", t => t.Estudianteid, cascadeDelete: true)
                .ForeignKey("dbo.Libro", t => t.Libroid, cascadeDelete: true)
                .Index(t => t.Estudianteid)
                .Index(t => t.Libroid);
            
            CreateTable(
                "dbo.Estudiante",
                c => new
                    {
                        Estudianteid = c.Int(nullable: false, identity: true),
                        Nombre = c.String(maxLength: 100, unicode: false),
                        Apellido = c.String(maxLength: 100, unicode: false),
                        Direccion = c.String(maxLength: 100, unicode: false),
                        Telefono = c.String(maxLength: 100, unicode: false),
                        borrado = c.Boolean(nullable: false),
                        Estatus = c.String(maxLength: 2, unicode: false),
                        FechaRegistro = c.DateTime(nullable: false, storeType: "date"),
                        FechaModificacion = c.DateTime(nullable: false, storeType: "date"),
                    })
                .PrimaryKey(t => t.Estudianteid);
            
            CreateTable(
                "dbo.Devolucion",
                c => new
                    {
                        Devolucionid = c.Int(nullable: false, identity: true),
                        Prestamoid = c.Int(nullable: false),
                        borrado = c.Boolean(nullable: false),
                        Estatus = c.String(maxLength: 2, unicode: false),
                        FechaRegistro = c.DateTime(nullable: false, storeType: "date"),
                        FechaModificacion = c.DateTime(nullable: false, storeType: "date"),
                    })
                .PrimaryKey(t => t.Devolucionid)
                .ForeignKey("dbo.Prestamo", t => t.Prestamoid, cascadeDelete: true)
                .Index(t => t.Prestamoid);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Devolucion", "Prestamoid", "dbo.Prestamo");
            DropForeignKey("dbo.Libro", "Autorid", "dbo.Autor");
            DropForeignKey("dbo.Prestamo", "Libroid", "dbo.Libro");
            DropForeignKey("dbo.Prestamo", "Estudianteid", "dbo.Estudiante");
            DropForeignKey("dbo.Libro", "Editorialid", "dbo.Editorial");
            DropIndex("dbo.Devolucion", new[] { "Prestamoid" });
            DropIndex("dbo.Prestamo", new[] { "Libroid" });
            DropIndex("dbo.Prestamo", new[] { "Estudianteid" });
            DropIndex("dbo.Libro", new[] { "Editorialid" });
            DropIndex("dbo.Libro", new[] { "Autorid" });
            DropTable("dbo.Devolucion");
            DropTable("dbo.Estudiante");
            DropTable("dbo.Prestamo");
            DropTable("dbo.Editorial");
            DropTable("dbo.Libro");
            DropTable("dbo.Autor");
        }
    }
}
