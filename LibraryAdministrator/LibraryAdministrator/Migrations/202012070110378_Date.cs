﻿namespace LibraryAdministrator.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Date : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Prestamo", "Fecha_Entrega", c => c.DateTime(nullable: false, storeType: "date"));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Prestamo", "Fecha_Entrega", c => c.DateTime(nullable: false));
        }
    }
}
