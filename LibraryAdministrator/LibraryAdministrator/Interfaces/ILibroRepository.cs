﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibraryAdministrator.Core;
using LibraryAdministrator.DBModel.entities;

namespace LibraryAdministrator.Interfaces
{
    interface ILibroRepository : IGenericRepository<Libro>
    {
    }
}
