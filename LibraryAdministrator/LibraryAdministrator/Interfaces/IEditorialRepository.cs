﻿using LibraryAdministrator.Core;
using LibraryAdministrator.DBModel.entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryAdministrator.Interfaces
{
    interface IEditorialRepository : IGenericRepository<Editorial>
    {
    }
}
