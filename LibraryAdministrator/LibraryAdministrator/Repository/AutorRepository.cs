﻿using LibraryAdministrator.DBModel.Dbcontext;
using LibraryAdministrator.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibraryAdministrator.Core;
using LibraryAdministrator.DBModel.entities;

namespace LibraryAdministrator.Repository
{
    public class AutorRepository : GenericRepository<Autor>, IAutorRepository
    {
     
    }
}
