﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibraryAdministrator.Core;
using LibraryAdministrator.DBModel.entities;
using LibraryAdministrator.Interfaces;

namespace LibraryAdministrator.Repository
{
    class EditorialRepository: GenericRepository<Editorial>,IEditorialRepository
    {
    }
}
