﻿using LibraryAdministrator.DBModel.Dbcontext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryAdministrator.Core
{
    public interface IGenericRepository<T>
    {
        T Create(T model);
        List<T> GetAll();
        T FindbyID(int id);
        OperationResult Update(T model);
        OperationResult Delete(T model);
    }
}
