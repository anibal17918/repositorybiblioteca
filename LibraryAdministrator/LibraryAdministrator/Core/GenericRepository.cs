﻿using LibraryAdministrator.DBModel.Dbcontext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;

namespace LibraryAdministrator.Core
{
    public class GenericRepository<T> : IGenericRepository<T> where T:BaseEntity
    {
        private LibraryDbcontext _context;
        private DbSet<T> _set;

        public GenericRepository()
        {
            _context = new LibraryDbcontext();
            _set = _context.Set<T>();
        }
        public T Create(T model)
        {
            _set.Add(model);
            _context.SaveChanges();

            return model;
        }

        public OperationResult Delete(T model)
        {
            _context.Entry(model).State = EntityState.Modified;
            model.borrado = true;
            _context.SaveChanges();

            return new OperationResult() { Result = true };
        }

        public T FindbyID(int id)
        {
            return _context.Set<T>().FirstOrDefault(p => p.id == id);
        }

        public List<T> GetAll()
        {
            return _set.Where(p => p.borrado == false).ToList();
        }

        public OperationResult Update(T model)
        {
            var model1 = FindbyID(model.id);
            _context.Entry(model1).State = EntityState.Modified;
            _context.Entry(model1).CurrentValues.SetValues(model);
            _context.SaveChanges();

            return new OperationResult() { Result = true };
        }
    }
}
