﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using LibraryAdministrator.DBModel.entities;


namespace LibraryAdministrator.DBModel.Dbcontext
{
    class LibraryDbcontext:DbContext
    {
        public LibraryDbcontext()
        : base("name=MSSQL")
        {

        }
        public DbSet<Autor> Autor { get; set; }
        public DbSet<Devolucion> Devolucion { get; set; }
        public DbSet<Editorial> Editorial { get; set; }
        public DbSet<Estudiante> Estudiante { get; set; }
        public DbSet<Libro> Libro { get; set; }
        public DbSet<Prestamo> Prestamo { get; set; }
        

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            

            //Autor
            modelBuilder.Entity<Autor>()
           .ToTable("Autor")
           .HasKey(p => p.id);

            modelBuilder.Entity<Autor>()
            .HasMany(p => p.Libros)
            .WithRequired(p => p.Autor)
            .HasForeignKey(p => p.Autorid);

            modelBuilder.Entity<Autor>()
            .Property(p => p.Nombre)
            .HasMaxLength(100).IsUnicode(false);

            modelBuilder.Entity<Autor>()
            .Property(p => p.Apellido)
            .HasMaxLength(100).IsUnicode(false);

            modelBuilder.Entity<Autor>()
            .Property(p => p.Nacionalidad)
            .HasMaxLength(100).IsUnicode(false);

            modelBuilder.Entity<Autor>()
            .Property(p => p.Estatus)
            .HasMaxLength(2).IsUnicode(false);

            modelBuilder.Entity<Autor>()
            .Property(p => p.FechaModificacion)
            .HasColumnType("Date");

            modelBuilder.Entity<Autor>()
            .Property(p => p.FechaRegistro)
            .HasColumnType("Date");


            modelBuilder.Entity<Autor>()
            .Property(p => p.id).HasColumnName("Autorid");

            //Devolucion
            modelBuilder.Entity<Devolucion>()
            .ToTable("Devolucion")
            .HasKey(p => p.id);

                     

            modelBuilder.Entity<Devolucion>()
            .Property(p => p.id).HasColumnName("Devolucionid");

            modelBuilder.Entity<Devolucion>()
           .Property(p => p.Estatus)
           .HasMaxLength(2).IsUnicode(false);

            modelBuilder.Entity<Devolucion>()
            .Property(p => p.FechaModificacion)
            .HasColumnType("Date");

            modelBuilder.Entity<Devolucion>()
            .Property(p => p.FechaRegistro)
            .HasColumnType("Date");

            //Editorial
            modelBuilder.Entity<Editorial>()
            .ToTable("Editorial")
            .HasKey(p => p.id);

            modelBuilder.Entity<Editorial>()
            .Property(p => p.id).HasColumnName("Editorialid");

            modelBuilder.Entity<Editorial>()
           .Property(p => p.Nombre)
           .HasMaxLength(500).IsUnicode(false);

            modelBuilder.Entity<Editorial>()
           .Property(p => p.Direccion)
           .HasMaxLength(500).IsUnicode(false);

            modelBuilder.Entity<Editorial>()
           .Property(p => p.Estatus)
           .HasMaxLength(2).IsUnicode(false);

            modelBuilder.Entity<Editorial>()
            .Property(p => p.FechaModificacion)
            .HasColumnType("Date");

            modelBuilder.Entity<Editorial>()
            .Property(p => p.FechaRegistro)
            .HasColumnType("Date");

            //Estudiante
            modelBuilder.Entity<Estudiante>()
            .ToTable("Estudiante")
            .HasKey(p => p.id);

            modelBuilder.Entity<Estudiante>()
            .HasMany(p => p.Prestamo)
            .WithRequired(p => p.Estudiante)
            .HasForeignKey(p => p.Estudianteid);
            

            modelBuilder.Entity<Estudiante>()
            .Property(p => p.id).HasColumnName("Estudianteid");

            modelBuilder.Entity<Estudiante>()
            .Property(p => p.Nombre)
            .HasMaxLength(100).IsUnicode(false);

            modelBuilder.Entity<Estudiante>()
            .Property(p => p.Apellido)
            .HasMaxLength(100).IsUnicode(false);

            modelBuilder.Entity<Estudiante>()
            .Property(p => p.Direccion)
            .HasMaxLength(100).IsUnicode(false);

            modelBuilder.Entity<Estudiante>()
            .Property(p => p.Telefono)
            .HasMaxLength(100).IsUnicode(false);

            modelBuilder.Entity<Estudiante>()
           .Property(p => p.Estatus)
           .HasMaxLength(2).IsUnicode(false);

            modelBuilder.Entity<Estudiante>()
            .Property(p => p.FechaModificacion)
            .HasColumnType("Date");

            modelBuilder.Entity<Estudiante>()
            .Property(p => p.FechaRegistro)
            .HasColumnType("Date");

            //Libros
            modelBuilder.Entity<Libro>()
            .ToTable("Libro")
            .HasKey(p => p.id);

            modelBuilder.Entity<Libro>()
            .HasMany(p => p.Prestamo)
            .WithRequired(p => p.Libro)
            .HasForeignKey(p => p.Libroid);
            
                
            modelBuilder.Entity<Libro>()
            .Property(p => p.id).HasColumnName("Libroid");

            modelBuilder.Entity<Libro>()
           .Property(p => p.Nombre)
           .HasMaxLength(200).IsUnicode(false);

            modelBuilder.Entity<Libro>()
           .Property(p => p.Estatus)
           .HasMaxLength(2).IsUnicode(false);

            modelBuilder.Entity<Libro>()
            .Property(p => p.FechaModificacion)
            .HasColumnType("Date");

            modelBuilder.Entity<Libro>()
            .Property(p => p.FechaRegistro)
            .HasColumnType("Date");

            //Prestamo
            modelBuilder.Entity<Prestamo>()
            .ToTable("Prestamo")
            .HasKey(p => p.id);

            modelBuilder.Entity<Prestamo>()
            .Property(p => p.id).HasColumnName("Prestamoid");

            modelBuilder.Entity<Prestamo>()
           .Property(p => p.Estatus)
           .HasMaxLength(2).IsUnicode(false);

            modelBuilder.Entity<Prestamo>()
            .Property(p => p.FechaModificacion)
            .HasColumnType("Date");

            modelBuilder.Entity<Prestamo>()
            .Property(p => p.FechaRegistro)
            .HasColumnType("Date");

            modelBuilder.Entity<Prestamo>()
            .Property(p => p.Fecha_Entrega)
            .HasColumnType("Date");


        }
    }
}
