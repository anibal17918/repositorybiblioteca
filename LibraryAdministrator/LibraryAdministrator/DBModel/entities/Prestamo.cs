﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryAdministrator.DBModel.entities
{
    public class Prestamo : BaseEntity
    {
        public int Estudianteid { get; set; }
        public int Libroid { get; set; }
        public Libro Libro { get; set; }
        public DateTime Fecha_Entrega { get; set; }
        public Estudiante Estudiante { get; set; }
    }
}
