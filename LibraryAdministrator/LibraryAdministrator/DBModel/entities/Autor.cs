﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using LibraryAdministrator.DBModel.entities;

namespace LibraryAdministrator.DBModel.entities
{
    public class Autor:BaseEntity
    {
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Nacionalidad { get; set;}
        public ICollection<Libro> Libros { get; set; }
    }
}
