﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibraryAdministrator.DBModel.entities;

namespace LibraryAdministrator.DBModel.entities
{
    public class Estudiante : BaseEntity
    {
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Direccion { get; set; }
        public string Telefono { get; set; }

        public ICollection<Prestamo> Prestamo { get; set; }
    }
}
