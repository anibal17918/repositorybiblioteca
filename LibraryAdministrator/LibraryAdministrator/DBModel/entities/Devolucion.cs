﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryAdministrator.DBModel.entities
{
    public class Devolucion : BaseEntity
    {
        public int Prestamoid { get; set; }
        public Prestamo Prestamo { get; set; }
    }
}
