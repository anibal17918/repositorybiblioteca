﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryAdministrator.DBModel.entities
{
    public class Editorial : BaseEntity
    {
        public string Nombre { get; set; }
        public string  Direccion { get; set; }
        public ICollection<Libro> Libro { get; set; }
    }
}
