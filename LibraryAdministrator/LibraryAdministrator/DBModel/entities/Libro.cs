﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryAdministrator.DBModel.entities
{
    public class Libro:BaseEntity
    {
        public string Nombre { get; set; }
        public int Autorid { get; set; }
        public int Editorialid { get; set; }
        public Editorial Editorial { get; set; }
        public Autor Autor { get; set; }
        public ICollection<Prestamo> Prestamo { get; set; }

    }
}
